import request from "@/utils/request"

export function userLogin(loginInfo) {
  return request.post('login', loginInfo)
}
