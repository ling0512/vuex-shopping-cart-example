import Vue from "vue";
import Vuex, { createLogger } from "vuex";
// import Vuex from "vuex";

import cart from "./modules/cart";
import user from "./modules/user";
import products from "./modules/products";
import counter from './modules/counter'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  strict: debug,
  plugins: debug ? [createLogger()] : [],
  state: {
    // counter: {
    //   message: "Hello! I am Xiling!",
    // },
  },
  getters: {

  },
  mutations: {
    changeMsg(state, str) {
      state.counter.message += str;
    },
  },
  actions: {
    asyaddNum(content, obj) {
      // console.log(content);
      // setTimeout(() => {
        // content.commit("changeMsg", "actions提交的函数");
        // console.log(obj.data);
        // obj.success()
      // }, 1000);
      return new Promise( (resolve, reject) => {
        setTimeout(() => {
          content.commit("changeMsg", "actions提交的函数");
          resolve(content.state.counter)
        })
      })
    },
  },
  modules: {
    cart,
    user,
    products,
    counter
  },
});
