const state = () => ({
  userInfo: JSON.parse(window.sessionStorage.getItem('userInfo')) || {}
})

const getters = {
  getUserInfo: (state) => {
    return state.userInfo
  },
  isLogin: (state) => {
    console.log(state.userInfo.token ? true : false);
    return state.userInfo.token ? true : false
  }
}

const mutations = {
  setUserInfo(state, info) {
    console.log(info);
    window.sessionStorage.setItem('userInfo', JSON.stringify(info));
    state.userInfo = info;
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations
}