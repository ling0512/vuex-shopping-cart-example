const state = () => ({
  message: "hello",
  mutationsCount: 0,
  count: 0,
  todos: [
    {
      id: 1,
      text: "写日报",
      done: true,
    },
    {
      id: 2,
      text: "看一篇英文文章",
      done: false,
    },
  ],
});

// getters
const getters = {
  doneTodos: (state) => {
    return state.todos.filter((todo) => todo.done);
  },
  doneTodosCount: (state, getters) => {
    // console.log("state", state);
    // console.log("doneTodosCount", getters);
    // console.log(getters.doneTodos);
    return getters.doneTodos.length;
  },
  reverseStr: (state) => {
    return state.message.split("").reverse().join();
  },
  getCount: (state) => {
    console.log(state);
    return state.mutationsCount
  }
};

// mutations
const mutations = {
  mIncrement(state, n) {
    state.mutationsCount += n;
  },
  // 通过对象传递
  mDecrement(state, payload) {
    state.mutationsCount -= payload.amount;
  },
  increment(state) {
    state.count ++
  },
  decrement(state) {
    state.count --
  }
};

const actions = {
  increment: ({
    commit
  }) => commit('increment'),
  decrement: ({
    commit
  }) => commit('decrement')
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
