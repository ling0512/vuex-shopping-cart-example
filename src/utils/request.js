import axios from "axios";
import qs from "qs";
import { Message } from "@/elementui/index";
import "nprogress/nprogress.css";
import NProgress from "nprogress";

const service = axios.create({
  baseURL: "http://127.0.0.1:8888/api/private/v1/",
  timeout: 5000,
  withCredentials: false,
  // headers: { 'Content-Type': 'application/json' }
});

// axios
// 1. 请求拦截器
service.interceptors.request.use(
  (config) => {
    NProgress.start();
    // if (config.method === 'get' || config.method === 'delete') {

    // }
    config.headers.Authorization = window.sessionStorage.getItem("token");
    return config;
  },
  (err) => {
    throw new Error(err);
  }
);

// 2. 响应拦截器
service.interceptors.response.use(
  (response) => {
    NProgress.done();
    return response;
  },
  (err) => {
    return Promise.reject(err);
  }
);

export default service;
