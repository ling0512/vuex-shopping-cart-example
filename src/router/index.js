import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
import { Message } from "element-ui";
// import { mapGetters } from "vuex";
import Store from "@/store/index";

Vue.use(VueRouter);
Vue.component(Message.name, Message)

const ProductList = () => import('@/views/ProductList')
const ShoppingCart = () => import('@/views/ShoppingCart')
const Login = () => import('@/views/Login')

const routes = [
  // {
  //   path: "/",
  //   name: "home",
  //   component: HomeView,
  // },
  {
    path: "/",
    name: "ProductList",
    component: ProductList,
  },
  {
    path: "/shoppingCart",
    name: "ShoppingCart",
    component: ShoppingCart,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  // {
    // path: "/about",
    // name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () =>
      // import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  // },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// ...mapGetters('user', {
//   userInfo: 'userInfo'
// })

router.beforeEach((to, from, next) => {
  const userInfo = Store.getters['user/getUserInfo']
  // .this.$store

  if (to.name !== 'Login' && !userInfo.token ) {
    Message.warning('身份认证失效，请先登录。')
    next({
      name: "Login"
    })
  } else {
    next()
  }
})

export default router;
